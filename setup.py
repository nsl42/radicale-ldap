#!/usr/bin/env python3

from distutils.core import setup

setup(name="radicale_ldap",
              packages=["radicale_ldap"])
