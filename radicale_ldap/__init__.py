from radicale.auth import BaseAuth
from radicale.log import logger
from ldap3 import Server, Connection, ALL

PLUGIN_CONFIG_SCHEMA = {"auth": {
    "ldap_url": {"value": "", "type": str},
    "ldap_base": {"value": "", "type": str},
    "ldap_attribute": {"value": "", "type": str},
    "ldap_binddn": {"value": "", "type": str},
    "ldap_password": {"value": "", "type": str}

    }}


class Auth(BaseAuth):
    def __init__(self, configuration):
        super().__init__(configuration.copy(PLUGIN_CONFIG_SCHEMA))

    def login(self, login, password):
        # Get password from configuration option

        ldap_url = self.configuration.get("auth", "ldap_url")
        ldap_base = self.configuration.get("auth", "ldap_base")
        ldap_attribute = self.configuration.get("auth", "ldap_attribute")
        ldap_binddn = self.configuration.get("auth", "ldap_binddn")
        ldap_password = self.configuration.get("auth", "ldap_password")
        # Check authentication
        logger.info("+++++ \nStarting login to url %r by %r with password %r", ldap_url, login, password)

        server = Server(ldap_url)
        dn = ldap_attribute + "="+ login+","+ldap_base
        logger.info("Connecting to ldap server %r, using %r as dn", ldap_url, dn)
        try:
            conn = Connection(server, dn, password, auto_bind=True)
            logger.error(conn.extend.standard.who_am_i())
            return login
        except:
            logger.error("The LDAP connection failed")
            logger.error("The LDAP connection was not done")
            return ""
